# Welcome to new-developer_v3

Brief guide to solve the test


## 1

-   clone the project and cd to it

    `git clone https://gitlab.com/CarlosOchoa8/new-developer_v3.git` \
    `cd new-developer_v3`
    
-   create docker container and up it
    ```
    docker compose build --no-cache && docker compose up
    ```

Test will run automatically in another container called `test_runner`
## 2

-   access to container 
    ```
    docker exec -it developer_v3 bash
    ```

-   to read responses, access to each readme according to instruction given
    ```
    cat README-{number}.md
    ```
