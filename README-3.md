
# Part3
Due my projects aren't deployed yet or are in development, i can't share them properly, although i'll share some piece of code of them, and their public repository.

### e-learning platform [https://github.com/CarlosOchoa8/e-learning_api]

    router = APIRouter()


    @router.post('/create/', description='Append an image into a lesson.',
                response_model=schemas.LessonFileInDBSchema)
    async def create(lesson_file_in: schemas.LessonFileCreateSchema, files_in: list[UploadFile] = File(default=None),
                    db: Session = Depends(get_db)):
        new_files = await crud.lesson_file.create(obj_in=lesson_file_in, file_in=files_in, db=db)
        return schemas.LessonFileInDBSchema(**new_files.__dict__)


    @router.put('/update/{file_id}', description='Update lesson image.',
                response_model=schemas.LessonFileInDBSchema)
    def update(lesson_file_in: schemas.LessonFileUpdateSchema, file_id: int, db: Session = Depends(get_db)):
        if lesson_file := crud.lesson_file.get(id=file_id, db=db):
            try:
                files_updated = crud.lesson_file.update(db=db, db_obj=lesson_file, obj_in=lesson_file_in)
                return schemas.LessonFileInDBSchema(**files_updated.__dict__)
            except Exception as e:
                raise HTTPException(
                    status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                    detail='A problem has occurred while trying to update files.'
                ) from e
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail='The lesson files does not exists.')


### ukituki-store [https://github.com/CarlosOchoa8/uki-store-app]

    @users_blueprint.route("/account", methods=["GET", "POST"])
    @get_current_user
    def my_account():
        form = UserUpdateForm()
        form_in = form.data

    if request.method == "POST" and form.validate_on_submit():
        if not User.query.filter(User.email == form_in["email"] or
                                        User.phone_number == form_in["phone_number"]).first():
            db_user = User.query.filter_by(email=g.user.email).first()
            user_crud.update(obj_in=form_in, db_obj=db_user)
            return redirect(url_for("index"))

        else:
            error = (f"El correo {form.data['email']} o teléfono {form.data['phone_number']} "
                     f"ya se encuentran registrados.")
            flash(error)
    return render_template("users/account/my_account.html", form=form)