
# Part2

## Most representative concepts of CI


#### Single Source Repository
-    Work only in one repository per project, allowing to share the projects changes in one centralized repository and taking advantage of Version Control Systems. Also provides ease on management of code, collaboration, continuos integration, consistency of project structure.

#### Automating the build
-    Configuration of the central program from the original source, taking the last changes made in the project and, with automated processes review them with the purpose of track some possible errors in the current code caused from the last changes.

#### Automated Testing
-    Running automatically test written over the last changes made, controlling the by the test the functionality that was affected. This way running test after commits allows  to find bugs, catch uncontrolled situations in the feature purpose, and then, work in fixing them.

#### Automated deployment
-   Process where finally the code actualizations are implemented in production server, after been tested and validated according business needs. Avoiding manual intervention, accelerating software delivery and reducing risk of failures mades by humans.