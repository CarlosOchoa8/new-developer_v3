import random


def input_manual_list():
    input_list = input("Enter the list of integers separated by spaces: ")
    collection = list(map(int, input_list.split()))
    collection.sort()
    return collection


def generate_random_list(length: int):
    return sorted([random.randint(1, length) for _ in range(length)])


def find_matching_pair(sum_result: int, collection: list[int]):
    start = 0
    end = len(collection) - 1
    while start < end:
        current_sum = collection[start] + collection[end]
        if current_sum == sum_result:
            return f"OK, matching pair ({collection[start]}, {collection[end]})"
        elif current_sum < sum_result:
            start += 1
        else:
            end -= 1

    return "No"


if __name__ == "__main__":
    while True:
        option = input("Do you want to: manually input a list of numbers (M) generate a random list (A) exit (E)? ").upper()
        
        if option == 'M':
            numbers = input_manual_list()
        elif option == 'A':
            length = int(input("Enter the length of the random list: "))
            numbers = generate_random_list(length)
        elif option == 'E':
            break
        else:
            print("Unvalid option.")
            continue

        target_sum = int(input("Enter the target sum "))
        print(find_matching_pair(target_sum, numbers))