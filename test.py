from part4 import find_matching_pair
import unittest


mock_list1 = [2,3,6,7]
mock_list2 = [1,3,3,7]


class TestFindMatchPair(unittest.TestCase):
    
    def test_find_matching_pair(self):
        self.assertEqual(find_matching_pair(9, mock_list1), f"OK, matching pair (2, 7)", "This shouldn't been happen.")
        self.assertEqual(find_matching_pair(9, mock_list2), f"No", "This shouldn't been happen.")


if __name__ == "__main__":
    unittest.main()