FROM python:3.11.4-slim

WORKDIR /usr/src/app

RUN pip install --upgrade pip

COPY entrypoint.sh .
RUN chmod +x entrypoint.sh

COPY . .

EXPOSE 5555

CMD ["./entrypoint.sh"]
